from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask import Flask

from config import Config


app = Flask(__name__)
app.config.from_object(Config)

db = SQLAlchemy(app)
migrate = Migrate(app, db)


from modules.cian import commands
from modules.avito import commands
from modules.dataset import commands, models


if __name__ == "__main__":
    app.run()
