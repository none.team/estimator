import click
from flask.cli import AppGroup

from application import app


avito = AppGroup("avito")

@avito.command("sync")
def avito_sync():
    app.logger.info("avito_sync")

app.cli.add_command(avito)